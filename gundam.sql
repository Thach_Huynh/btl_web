-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 17, 2021 lúc 06:21 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 7.4.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `gundam`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(5, 'thach', '7b5dd5da9ee8f40b54853d0d8c901e34fad7056d');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill`
--

CREATE TABLE `bill` (
  `idBill` int(10) NOT NULL,
  `idCustomer` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` float NOT NULL,
  `status` int(10) NOT NULL,
  `payment` int(10) NOT NULL,
  `payment_status` int(10) NOT NULL,
  `dateBill` datetime NOT NULL,
  `Note` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bill`
--

INSERT INTO `bill` (`idBill`, `idCustomer`, `name`, `phone`, `email`, `address`, `total`, `status`, `payment`, `payment_status`, `dateBill`, `Note`) VALUES
(1, 3, 'Thach', '0984141206', 'thachhuynh@gmail.com', 'Khanh Hoa', 200000, 1, 1, 1, '2021-11-03 04:16:03', ''),
(2, 3, 't', '01212', '2424', '24242', 200000, 1, 0, 1, '2021-11-03 04:18:07', ''),
(3, 3, 't', '01212', '2424', '24242', 200000, 1, 0, 1, '2021-11-03 04:18:07', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_detail`
--

CREATE TABLE `bill_detail` (
  `id` int(10) NOT NULL,
  `idBill` int(10) NOT NULL,
  `idMenu` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bill_detail`
--

INSERT INTO `bill_detail` (`id`, `idBill`, `idMenu`, `quantity`, `price`) VALUES
(42, 14, 5, 10, 150000),
(47, 18, 2, 9, 17000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `idCustomer` int(10) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nameCustomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phoneCustomer` int(20) NOT NULL,
  `emailCustomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressCustomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalBill` int(10) NOT NULL,
  `typeCustomer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`idCustomer`, `username`, `password`, `nameCustomer`, `phoneCustomer`, `emailCustomer`, `addressCustomer`, `totalBill`, `typeCustomer`) VALUES
(1, 'phuc', '9229ec79a248d8810d401bdf4febed623264819d', 'Trần Nguyễn Bảo Phúc', 986245128, 'baophuc@gmail.com', 'Tư Nghĩa, Quảng Ngãi', 0, 0),
(2, 'minh', 'b29025a795c4f5ea28fd2d7270a5600fe8099077', 'P', 987456129, 'minhpham@gmail.com', 'Gò Vấp, TP.HCM', 0, 0),
(3, 'thach', 'a20232a08cda98c9dfdcccf66b12d06e884a8a8c', 'Huỳnh Nguyên Thạch', 984141206, 'huynhthach@gmail.com', 'Cam Ranh, Khánh Hoà', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employees`
--

CREATE TABLE `employees` (
  `idEmployees` int(11) NOT NULL,
  `nameEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imageEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phoneEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `employees`
--

INSERT INTO `employees` (`idEmployees`, `nameEmployees`, `imageEmployees`, `emailEmployees`, `phoneEmployees`, `addressEmployees`) VALUES
(2, 'Phạm Minh Triết ', 'NV001.png', 'trietpham2107@gmail.com', '0849277508', 'Thành Phố HCM');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(10) NOT NULL,
  `nameMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typeMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priceMenu` float NOT NULL,
  `imageMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `noteMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`idMenu`, `nameMenu`, `typeMenu`, `priceMenu`, `imageMenu`, `unitMenu`, `noteMenu`) VALUES
(10, 'Gundam BARBATOS', 'HG', 1680000, 'Untitled.png', 'Bộ', 'Gundam siu xịn'),
(11, 'GUNDAM BARBATOS LUPUS REX ( HGIBO - 1/144 ) - MÔ HÌNH GUNPLA CHÍNH HÃNG BANDAI', 'HG', 490000, 'gundam_barbatos_lupus_rex_hgibo_mo_hinh_gunpla_chinh_hang_bandai_d1bbe1da55cb4aa384deb6ad4c6f4c94_master.jpg', 'Bộ', '   Gundam Barbatos Lupus Rex (HGIBO - 1/144) đang có bán tại cửa hàng nShop, ngôi sao lớn của series Gundam Iron-Blooded Orphans, một \"mãnh thú\" đúng nghĩa về cả sức mạnh lẫn phong cách tấn công bạo liệt. . \r\n'),
(12, 'Narrative Gundam A-Packs (HGUC)', 'HG', 1100000, '212404279341c6c-bf9a-47e5-91ca-a6cf3e8e4949-jpeg.jpg', 'Bộ', ' \r\nĐộ chi tiết vừa phải, khớp chuyển động tương đối linh hoạt. Ráp theo kiểu bấm khớp, không cần dùng keo dán\r\nThuộc Series: Gundam Build Divers\r\nTrang bị: Rifle, Lance, Shield\r\nNhiều biểu cảm khuôn mặt\r\nDecal dán (Xem hướng dẫn dán decal)\r\nKèm đế dựng \r\n'),
(13, 'Destiny Gundam (Heine Westenfluss Custom) (HGCE)', 'HG', 490000, '40171760-jpeg.jpg', 'Bộ', '\r\nĐộ chi tiết vừa phải, khớp chuyển động tương đối linh hoạt. Ráp theo kiểu bấm khớp, không cần dùng keo dán\r\nDecal dán (Xem hướng dẫn dán decal)\r\nSản phẩm Gunpla chính hãng của Bandai'),
(14, 'Mô Hình Lắp Ráp HG Cybaster Super Robot Wars 1/144', 'HG', 700000, '10757212p.jpg', 'Bộ', 'Sản Phẩm Nhựa Cao Cấp Với Độ Sắc Nét Cao\r\nSản Xuất Bởi Bandai\r\nAn Toàn Với Trẻ Em\r\nPhát Triển Trí Não Cho Trẻ Hiệu Quả Đi Đôi Với Niềm Vui Thích Bất Tận\r\nRèn Luyện Tính Kiên Nhẫn Cho Người Chơi\r\nPhân Phối Bởi Gundamstorevn'),
(15, 'MÔ HÌNH GUNDAM BANDAI 1/144 HG AHEAD SERIE HG00 GUNDAM 00', 'HG', 490000, '81niub-q-plus-zl-sl1500.jpg', 'Bộ', 'MÔ HÌNH GUNDAM BANDAI 1/144 HG AHEAD SERIE HG00 GUNDAM 00'),
(16, 'Gundam Bandai Hg Full Armor Rx-78 Thunderbolt 1/144 ', 'HG', 500000, '10371633p.jpg', 'Bộ', 'Sản phậm nhựa cao cấp với độ sắc nét cao\r\nSản xuất bởi Bandai Namco – Nhật Bản\r\nAn toàn với trẻ em\r\nPhát triển trí não cho trẻ hiệu quả đi đôi với niềm vui thích bất tận\r\nRèn luyện tính kiên nhẫn cho người chơi\r\nPhân phối bởi GundamstoreVN'),
(17, 'MÔ HÌNH GUNDAM BANDAI 1/144 HG SUPER FUMINA AXIS ANGEL Ver SERIE HGBF GUNDAM BUILD FIGHTERS', 'HG', 700000, '10443940p.jpg', 'Bộ', 'Sản phậm nhựa cao cấp với độ sắc nét cao\r\nSản xuất bởi Bandai Namco – Nhật Bản\r\nAn toàn với trẻ em\r\nPhát triển trí não cho trẻ hiệu quả đi đôi với niềm vui thích bất tận\r\nRèn luyện tính kiên nhẫn cho người chơi\r\nPhân phối bởi GundamstoreVN'),
(18, 'MÔ HÌNH LẮP RÁP BANDAI 1/144 HG RCX-76-02 GUNCANNON FIRST TYPE [IRON CAVALRY SQUADRON]', 'HG', 490, '10413724p.jpg', 'Bộ', 'Sản phậm nhựa cao cấp với độ sắc nét cao\r\nSản xuất bởi Bandai Namco – Nhật Bản\r\nAn toàn với trẻ em\r\nPhát triển trí não cho trẻ hiệu quả đi đôi với niềm vui thích bất tận\r\nRèn luyện tính kiên nhẫn cho người chơi\r\nPhân phối bởi GundamstoreVN'),
(19, 'MÔ HÌNH GUNDAM BANDAI 1/144 HG MISS SAZABI SERIE HGBF GUNDAM BUILD FIGHTERS', 'HG', 500000, '10260038p.jpg', 'Bộ', 'Sản phậm nhựa cao cấp với độ sắc nét cao\r\nSản xuất bởi Bandai Namco – Nhật Bản\r\nAn toàn với trẻ em\r\nPhát triển trí não cho trẻ hiệu quả đi đôi với niềm vui thích bất tận\r\nRèn luyện tính kiên nhẫn cho người chơi\r\nPhân phối bởi GundamstoreVN'),
(20, 'RG Force Impulse Titanium Finish The Gundam Base Limited Bandai 1/144', 'RG', 1700000, 'z2575965222530-8d0381195d37f4e05bafbf27b30d3e0d.jpg', 'Bộ', ' Tên Sản Phẩm : Mô Hình Lắp Ráp RG Force Impulse Titanium Finish The Gundam Base Limited Bandai 1/144 Đồ Chơi Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Rg\r\n\r\nTỷ Lệ : 1:144\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14\r\n\r\nKích Thước Hộp : 33x23x13 Cm / 56'),
(21, 'RG QanT Full Saber Trans-Am Clear Color Gundam Base Limited Bandai 1/144', 'RG', 1300000, 'chieucao-pg-mg-rg-hg-sd-b3316619-e40e-4e72-a594-0c974747ee3f.jpg', 'Bộ', ' Tên Sản Phẩm : Mô Hình Lắp Ráp RG QanT Full Saber Trans-Am Clear Color Gundam Base Limited Bandai 1/144 Đồ Chơi Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Rg\r\n\r\nTỷ Lệ : 1:144\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14 '),
(22, 'Bandai Mô Hình Rg Evangelion Unit 00 EVA00 1/144 ', 'RG', 1100000, '10673815p-copy.jpg', 'Bộ', ' Tên Sản Phẩm : Bandai Mô Hình Rg Evangelion Unit 00 EVA00 1/144 Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Rg\r\n\r\nTỷ Lệ : 1:144\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14 '),
(23, 'RG Zeong Last Shooting Effect Set 1/144', 'RG', 2000000, '10730028p.jpg', 'Bộ', 'Tên Sản Phẩm : Bandai Mô Hình Gundam RG Zeong Last Shooting Effect Set 1/144 Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Rg\r\n\r\nTỷ Lệ : 1:144\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(24, 'Gundam RG Nu HWS RX-93 P-Bandai 1/144 ', 'RG', 1700000, 'hakoe-14-copy.jpg', 'Bộ', ' Tên Sản Phẩm : Bandai Mô Hình Gundam RG Nu HWS RX-93 P-Bandai 1/144 Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Rg\r\n\r\nTỷ Lệ : 1:144\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14 '),
(25, 'RG Sky Grasper Launcher / Sword Pack Fx550 Gundam Seed ', 'RG', 700000, '10169347b10.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM 1/144 RG FX550 SKY GRASPER LAUNCHER/SWORD PACK SERIE GUNDAM SEED\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN\r\n\r\nPHIÊN BẢN : RG\r\n\r\nTỶ LỆ : 1:144\r\n\r\nMÃ SP :\r\n\r\nĐỘ TUỔI : >14'),
(26, 'Gundam RG RX-93 Nu Clear Color P-Bandai 1/144 Real Grade ', 'RG', 1700000, '10678053c53f3061692e-copy.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình Gundam RG RX-93 Nu Clear Color P-Bandai 1/144 Real Grade Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Rg\r\n\r\nTỷ Lệ : 1:144\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(27, 'Gundam Bandai RG GP01 Zephyranthes Gundam 0083 UC', 'RG', 700000, '10221394p.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM 1/144 RG GP01 ZEPHYRANTHES SERIE GUNDAM 0083\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN\r\n\r\nPHIÊN BẢN : RG\r\n\r\nTỶ LỆ : 1:144\r\n\r\nMÃ SP :\r\n\r\nĐỘ TUỔI : >14'),
(28, 'Gundam Bandai RG Red Frame Astray 1/144 Seed', 'RG', 700000, '20150706165222e8c.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM 1/144 RG ASTRAY RED FRAME SERIE GUNDAM SEED ASTRAY\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN\r\n\r\nPHIÊN BẢN : RG\r\n\r\nTỶ LỆ : 1:144\r\n\r\nMÃ SP :\r\n\r\nĐỘ TUỔI : >14'),
(29, 'Gundam MG Blaze Zaku Phantom Rey Za Burel Custom 1/100 P-Bandai', 'MG', 1700000, '1-copy-8c73f5ba-dfe5-485e-adcc-29d96cb6770e.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình Gundam MG Blaze Zaku Phantom Rey Za Burel Custom  1/100 P-Bandai Master Grade Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : MG\r\n\r\nTỷ Lệ : 1:100\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(30, 'P-Bandai Mô Hình Gundam MG Storm Bringer FA GM Turbulence 1/100 ', 'MG', 1700000, '5c9ba427-d634-4fa5-adf4-476e9db98e9b-jpeg.jpg', 'Bộ', 'Tên Sản Phẩm : P-Bandai Mô Hình Gundam MG Storm Bringer FA GM Turbulence 1/100 Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Mg\r\n\r\nTỷ Lệ : 1:100\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(31, '1/100 RX-78F00 Gundam Factory Yokohama ', 'MG', 1700000, '0811501635ffc2a19bee-copy.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình 1/100 RX-78F00 Gundam Factory Yokohama 1Bandai Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : MG\r\n\r\nTỷ Lệ : 1:100\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(32, 'Gundam MG Tallgeese 2 1/100 P-Bandai Master Grade ', 'MG', 1100000, '795e79a53515c04b9904.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình Gundam MG Tallgeese 2 1/100 P-Bandai Master Grade Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Mg\r\n\r\nTỷ Lệ : 1:100\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(33, 'Gundam P-Bandai 1/100 Mg Zaku Phantom Slash Yzak Custom Serie Gundam Seed', 'HG', 1700000, '20191127-mg-slash-zaku-phantom-02.jpg', 'Bộ', 'Tên Sản Phẩm :Đồ Chơi Lắp Ráp Anime Nhật Mô Hình Gundam P-Bandai 1/100 Mg Zaku Phantom Slash Yzak Custom Serie Gundam Seed\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Mg\r\n\r\nTỷ Lệ : 1:100\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(34, ' MG Shenlong Liao Ya Unit EW P-Bandai 1/100 ', 'MG', 1100000, 'gkgundamkit-mg-1100-xxxg-01s-shenlong-gundam-ew-liaoya-unit-8d6e74b6f2c20a7.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình Gundam MG Shenlong Liao Ya Unit EW P-Bandai 1/100 Master Grade Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Mg\r\n\r\nTỷ Lệ : 1:100\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(35, ' MG Catapult Base The Gundam Base Limited ', 'MG', 700000, 'imgrc0150052770.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình Lắp Ráp MG Catapult Base The Gundam Base Limited Gunpla Bandai Đồ Chơi Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nPhiên Bản : Mg\r\n\r\nTỷ Lệ :\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >14'),
(36, 'Gundam Bandai Mg Tallgeese 1/100 Wing Ew ', 'MG', 1100000, 'mg-tallgeese-box-art.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI 1/100 MG TALLGEESE SERIE GUNDAM WING\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN\r\n\r\nPHIÊN BẢN : MG\r\n\r\nTỶ LỆ : 1:100\r\n\r\nMÃ SP :\r\n\r\nĐỘ TUỔI : >14'),
(37, 'Bandai Mg Shenlong EW 1/100 Wing ', 'MG', 1700000, 'mg-ew-shenlong.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI 1/100 MG SHENLONG EW SERIE GUNDAM WING\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN\r\n\r\nPHIÊN BẢN : MG\r\n\r\nTỶ LỆ : 1:100\r\n\r\nMÃ SP :\r\n\r\nĐỘ TUỔI : >14'),
(38, 'Mg Gm Sniper Rgm-79g 1/100 Uc', 'MG', 1700000, '1079895-27767-55-pristine-copy.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI 1/100 MG SHENLONG EW SERIE GUNDAM WING\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN\r\n\r\nPHIÊN BẢN : MG\r\n\r\nTỶ LỆ : 1:100\r\n\r\nMÃ SP :\r\n\r\nĐỘ TUỔI : >14'),
(39, ' PG Color Clear Body Set 00 Raiser Bandai 1/60 ', 'PG', 4000000, 'pb-pg-00-raiser-color-clear-body-boxart-copy.jpg', 'Bộ', 'Tỉ lệ 1/60'),
(40, 'P-Bandai Mô Hình Gundam PG Phenex Narrative Ver 1/60 ', 'PG', 17000000, 'o1cn01ghi8pr1krk3lfq3o3-1022081160.jpg', 'Bộ', 'Hàng order'),
(41, 'Mô Hình Gundam SD Xi Bandai Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 490000, '10245737p.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình Gundam SD Xi Bandai Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản\r\n\r\nMã Sp :\r\n\r\nĐộ Tuổi : >10'),
(42, 'Gundam Bandai Sd Versal Knight Mô Hình Nhựa Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, '10329540p.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI SD VERSAL KNIGHT GUNDAM\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN'),
(43, 'Mô Hình SDW Cao Cao Wing Gundam Isei Style SD World Heroes Bandai Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, '10777724p.jpg', 'Bộ', 'Tên Sản Phẩm : Mô Hình SDW Cao Cao Wing Gundam Isei Style SD World Heroes Bandai Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản'),
(44, 'Bandai Sd Mazinkaiser SDCS Cross Silhouette Mô Hình Nhựa Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, '10595731p.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI SD CS MAZINKAISER\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN'),
(45, 'Gundam Bandai Sd Sun Jian Astray Tam Quốc Sangoku Soketsuden Mô Hình Nhựa Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, 'sdss-sun-jian-astray-gundam-2.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI SD SANGOKU SOKETSUDEN SUN JIAN ASTRAY GUNDAM\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN'),
(46, 'Gundam Bandai Sd Dong Zhuo Providence Tam Quốc Sangoku Soketsuden Mô Hình Nhựa Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, 'sdss-dong-zhuo-providence-gundam-1.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI SD SANGOKU SOKETSUDEN DONG ZHUO PROVIDENCE GUNDAM\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN'),
(47, 'Bandai SD Ground Type Gundam Rx-79G Sdcs Cross Silhouette Uc Mô Hình Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, 'sdcs-ground-type-gundam-1.jpg', 'Bộ', 'Tên Sản Phẩm : Bandai SD Ground Type Gundam Rx-79G Sdcs Cross Silhouette Uc Mô Hình Đồ Chơi Lắp Ráp Anime Nhật\r\n\r\nThương Hiệu : Bandai – Nhật Bản'),
(48, 'MÔ HÌNH GUNDAM BANDAI SD CS MAZINGER Z SERIE MAZINGER', 'SD', 300000, '10562470p.jpg', 'Bộ', 'TÊN SẢN PHẨM : MÔ HÌNH GUNDAM BANDAI SD CS MAZINGER Z SERIE MAZINGER\r\n\r\nTHƯƠNG HIỆU : BANDAI – NHẬT BẢN'),
(49, 'Gundam Bandai Sd Bug + Buduibing Set Tam Quốc Sangoku Soketsuden Mô Hình Đồ Chơi Lắp Ráp Anime Nhật', 'SD', 300000, 'sdss-bug-buduibing-set.jpg', 'Bộ', 'Gundam Bandai Sd Bug + Buduibing Set Tam Quốc Sangoku Soketsuden Mô Hình Đồ Chơi Lắp Ráp Anime Nhật'),
(50, 'GUNDAM BANDAI SD CAO CAO WING TAM QUỐC SANGOKU SOKETSUDEN MÔ HÌNH NHỰA ĐỒ CHƠI LẮP RÁP ANIME NHẬT', 'SD', 300000, 'sdss-cao-cao-wing-gundam-1.jpg', 'Bộ', 'GUNDAM BANDAI SD CAO CAO WING TAM QUỐC SANGOKU SOKETSUDEN MÔ HÌNH NHỰA ĐỒ CHƠI LẮP RÁP ANIME NHẬT');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`idBill`);

--
-- Chỉ mục cho bảng `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_detailfbk1` (`idBill`),
  ADD KEY `menu_ibfk_1` (`idMenu`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Chỉ mục cho bảng `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`idEmployees`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bill`
--
ALTER TABLE `bill`
  MODIFY `idBill` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `bill_detail`
--
ALTER TABLE `bill_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `idCustomer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `employees`
--
ALTER TABLE `employees`
  MODIFY `idEmployees` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

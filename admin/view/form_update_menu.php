<body>

    <head>
        <meta http-equiv="Content-Type" content="text/html" ; charset="UTF-8" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title>Update gundam</title>
        <style>
            form {
                font-family: 'Times New Roman', Times, serif;
                /* background-color: whitesmoke;*/
                border-collapse: collapse;
                width: 50%;
                border: 1px solid #000;
                margin-left: 25%;
                margin-top: 10%;
            }

            h2 {
                color: #17a2b8;
                padding-left: 20px;
            }

            input[type="text"] {
                width: 93%;
                height: 30px;
            }

            button[type="submit"] {
                font-family: 'Times New Roman', Times, serif;
                background-color: seagreen;
                margin-left: 40%;
                width: 100px;
                height: 40px;
                border: 1px solid #ddd;
                border-radius: 2px;
                margin-bottom: 20px;
                margin-top: 20px;
            }

            th,
            td {
                padding-left: 50px;
            }

            select {
                font-family: 'Times New Roman', Times, serif;
                width: 93%;
                height: 30px;
            }
        </style>
    </head>
</body>
<?php
header('Content-Type: text/html; charset=UTF-8');
$id = $_GET['idMenu'];
//Kết nối đến sever
include('../controller/connect.php');
//Truy vấn dữ liệu
$menu = ("select *from menu where idMenu='$id'") or die("Lỗi truy vấn");
$query = mysqli_query($conn, $menu);
$rs = mysqli_fetch_array($query);

?>
<form action="../controller/update_menu.php" method="POST">
    <h2>Update gundam</h2>
    <table>
        <input type="hidden" name="idMenu" value="<?php echo ($id); ?>">

        <tr>
            <th>Tên gundam:</th>
            <td><input type="text" name="nameMenu" value="<?php echo $rs['nameMenu']; ?>"></td>
        </tr>
        <tr>
            <th>Loại:</th>
            <td>
                <select name="typeMenu">
                    <option value="HG">HG</option>
                    <option value="RG">RG</option>
                    <option value="MG">MG</option>
                    <option value="PG">PG</option>
                    <option value="SD">SD</option>
                </select>
            </td>
        </tr>

        <tr>
            <th>Giá:</th>
            <td><input type="text" name="priceMenu" value="<?php echo $rs['priceMenu']; ?>"></td>
        </tr>
        <tr>
            <th>Hình ảnh</th>
            <td>
                <input type="file" id="myFile" name="imageMenu" value="<?php echo $rs['imageMenu']; ?>">
            </td>
        </tr>
        <tr>
            <th>Đơn vị tính:</th>
            <td>
                <select name="unitMenu">
                    <option value="Bộ">Bộ</option>
                    <option value="Runner">Runner</option>
                </select>
            </td>
        </tr>
        <tr>
            <th>Mô tả:</th>
            <td>
                <textarea name="noteMenu" rows="4" cols="90"> <?php echo $rs['noteMenu']; ?> </textarea>
            </td>
        </tr>
    </table>
    <button type="submit"><b>Update</b></button>
</form>
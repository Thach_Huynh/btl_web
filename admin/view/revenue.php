<style>
.revenue
{
	margin-left: 18%;
            margin-top: 5%;
}
</style>
<div class="revenue">
<div class="row">
	<div class="col-md-12">
		<h2 class="dashboard-title">BÁO CÁO KẾT QUẢ BÁN HÀNG</h2>
	</div>
	<div class="col-md-4">
		<div class="resport-box resport-blue">
			
			<div class="resport-data">
				<?php
					include('./controller/connect.php');
					date_default_timezone_set('Asia/Ho_Chi_Minh');
					$today= date('Y-m-d');
					$sql = "SELECT COUNT(*) AS totalcolum, SUM(total) AS total FROM bill WHERE payment_status='1' AND DaTe(dateBill)='$today'";
					$result= mysqli_query($conn,$sql);
					$row =mysqli_fetch_assoc($result);
				
				?>
				<p><b><?php echo $row['totalcolum'];?></b> Hóa đơn đã thanh toán trong ngày</p>
				<h4>Tổng doanh thu trong ngày <?php echo number_format($row['total'],0,',','.');?> đ</h4>
			</div>
		</div>
	</div>
	<div class="col-md-4">
		<div class="resport-box resport-green">
			
			<div class="resport-data">
				<?php
					include('./controller/connect.php');
					$sql = "SELECT COUNT(*) AS totalcolum, SUM(total) AS total FROM bill Where payment_status='1'";
					$result= mysqli_query($conn,$sql);
					$row =mysqli_fetch_assoc($result);
				?>
				<p> Tổng hóa đơn đã thanh toán:<b><?php echo $row['totalcolum'];?></b></p>
				<span>Tổng doanh thu: </span>
				<b><?php echo number_format($row['total'],0,',','.');?>đ</b>
				
			</div>
		</div>
	</div>
	
</div>
</div>
<div class="row">
	<canvas class="col-md-10" id="myChart"></canvas>
</div>
</div>
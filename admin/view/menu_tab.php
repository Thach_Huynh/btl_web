<body>
    <div id="layoutSidenav">
        <div id="layoutSidenav_nav">
            <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
                <div class="sb-sidenav-menu">
                    <div class="nav">
                        <a class="nav-link" href="index.php">
                            <div class="sb-nav-link-icon"></i></div>
                            Trang chủ
                        </a>
                        <a class="nav-link collapsed" href="index.php?pid=1">
                            <div class="sb-nav-link-icon"></div>
                            Quản lý hoá đơn
                        </a>
                        <li>
                            <a class="nav-link collapsed" href="index.php?pid=2" data-toggle="dropdown">
                                <div class="sb-nav-link-icon"></div>
                                Quản lý kho
                                <i class="fa fa-caret-down"></i>
                            </a>
                            <div class="dropdown-menu">
                                <a href="index.php?pid=2" class="dropdown-item">Tất cả</a>
                                <div class="dropdown-divider"></div>
                                <a href="index.php?pid=7" class="dropdown-item">HG</a>
                                <div class="dropdown-divider"></div>
                                <a href="index.php?pid=8" class="dropdown-item">RG</a>
                                <div class="dropdown-divider"></div>
                                <a href="index.php?pid=9" class="dropdown-item">MG</a>
                                <div class="dropdown-divider"></div>
                                <a href="index.php?pid=10" class="dropdown-item">PG</a>
                                <div class="dropdown-divider"></div>
                                <a href="index.php?pid=11" class="dropdown-item">SD</a>
                            </div>
                        </li>
                        <a class="nav-link collapsed" href="index.php?pid=3">
                            <div class="sb-nav-link-icon"></div>
                            Quản lý khách
                        </a>

                        <a class="nav-link collapsed" href="index.php?pid=14">
                            <div class="sb-nav-link-icon"></div>
                            Quản lý nhân viên
                        </a>
                        <a class="nav-link collapsed" href="index.php?pid=16">
                            <div class="sb-nav-link-icon"></div>
                            Quản lý doanh thu
                        </a>

                    </div>
                </div>
        </div>
        </nav>
    </div>
</body>
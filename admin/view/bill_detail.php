<style>
  button[type="submit"] {
    background-color: #ddd;
    margin-left: 85%;
    height: 35px;
    width: 155px;
    border: 1px solid #ddd;
    border-radius: 2px;
  }
</style>

<body class="sb-nav-fixed">
  <div id="layoutSidenav">
    <div id="layoutSidenav_content">
      <main>
        <div class="container-fluid">
          <h1 class="mt-4">Chi tiết về hoá đơn đã đặt</h1>
          <div class="card mb-4">
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                <?php

                // KẾT NỐI CSDL
                include('./controller/connect.php');
                $id = $_GET['idBill'];
                // TÌM TỔNG SỐ RECORDS
                $result = mysqli_query($conn, "select count(idBill) as total from bill_detail where idBill='$id'");
                $row = mysqli_fetch_array($result);
                $total_records = $row['total'];
                // echo "<pre/>";
                // var_dump($total_records);

                // TÌM LIMIT VÀ CURRENT_PAGE
                $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                $limit = 4;
                //  TÍNH TOÁN TOTAL_PAGE VÀ START
                // tổng số trang
                $total_page = ceil($total_records / $limit);

                // Giới hạn current_page trong khoảng 1 đến total_page
                if ($current_page > $total_page) {
                  $current_page = $total_page;
                } else if ($current_page < 1) {
                  $current_page = 1;
                }

                // Tìm Start
                $start = ($current_page - 1) * $limit;
                //     echo "<pre/>";
                // var_dump($result);
                ?>
                <?php
                // TRUY VẤN LẤY DANH SÁCH 
                // Có limit và start rồi thì truy vấn CSDL lấy danh sách món ăn
                include("./controller/connect.php");
                $sql = "SELECT * FROM bill_detail,menu WHERE bill_detail.idMenu =menu.idMenu AND  idBill='$id'  LIMIT $start, $limit";
                $query = mysqli_query($conn, $sql);

                ?>
                <?php
                include('./controller/connect.php');
                $output = "";
                if (isset($_POST['btnSearch'])) {
                  $search = $_POST['txtSearch'];
                  if ($search != "") {
                    $query = mysqli_query($conn, "SELECT * FROM bill_detail,menu WHERE bill_detail.idMenu =menu.idMenu  AND  (idMenu LIKE '%$search%'OR nameMenu LIKE '%$search%'AND  idBill='$id' )") or die("Lỗi truy vấn");
                    $result = mysqli_num_rows($query);
                    if ($result == 0) {
                      //Kết quả tìm thấy =0
                      $output .= "<span style='color:red;'>Không tìm thấy thông tin gundam  '$search'</span>";
                    } else {
                      //Kết quả tìm kiếm >0
                      $output .= "<span style='color:#555;'>Có $result kết quả được tìm thấy với từ khóa'$search'</span>" . "<br>";
                      while ($data = @mysqli_fetch_array($result)) {

                        echo '<tr>';
                        echo "<td>{$row['idMenu']}</td>";
                        echo "<td>{$row['nameMenu']}</td>";
                        echo "<td>{$row['quantity']}</td>";
                        echo "<td>{$row['priceMenu']}</td>";
                        echo "<td></td>";
                        echo '</tr>';
                      }
                    }
                  } else {
                    //Khong có ký tự nào ddc nhập
                    $output .= "<span style='color:red;'>Vui lòng nhập vào mã hoặc tên gundam cần tìm!</span>";
                  }
                }
                ?>
                <form action="" method="POST">
                  <input type="text" name="txtSearch" placeholder="Nhập mã hoặc tên gundam" />
                  <input type="submit" name="btnSearch" value="Search" />
                </form>
                <?php
                echo $output;
                ?>
                <thead>
                  <tr>
                    <th>Mã gundam</th>
                    <th>Tên gundam</th>
                    <th>Số lượng</th>
                    <th>Giá 1 sản phẩm</th>
                  </tr>
                </thead>
                <?php
                while ($data = mysqli_fetch_array($query)) {
                  $i = 1;
                  $id = $data['idBill'];
                ?>
                  <tr>
                  <tr>
                    <td>
                      <?php echo $data['idMenu'];  ?>
                    </td>
                    <td>
                      <?php echo $data['nameMenu'];  ?>
                    </td>
                    <td>
                      <?php echo $data['quantity'];  ?>
                    </td>
                    <td>
                      <?php echo $data['price'];  ?>
                    </td>
                  </tr>
                  </tr>
                <?php
                  $i++;
                }
                ?>
              </table>
              <?php
              // PHẦN HIỂN THỊ PHÂN TRANG
              // nếu current_page > 1 và total_page > 1 mới hiển thị nút Trở về
              if ($current_page > 1 && $total_page > 1) {
                echo '<a href="index.php?pid=11&&page=' . ($current_page - 1) . '">Trở về</a> ';
              }

              // Lặp khoảng giữa
              for ($i = 1; $i <= $total_page; $i++) {
                // Nếu là trang hiện tại thì hiển thị thẻ span
                // ngược lại hiển thị thẻ a
                if ($i == $current_page) {
                  echo '<button><span>' . $i . '</span></button> ';
                } else {
                  echo '<button><a href="index.php?pid=11&&page=' . $i . '">' . $i . '</a></button> ';
                }
              }
              // nếu current_page < $total_page và total_page > 1 mới hiển thị nút trang tiếp theo
              if ($current_page < $total_page && $total_page > 1) {
                echo '<a href="index.php?pid=11&&page=' . ($current_page + 1) . '">Trang tiếp theo</a> ';
              }
              ?>
            </div>
          </div>
        </div>
    </div>
    </main>
  </div>
  </div>
</body>
<style>
  button[type="submit"] {
    background-color: #ddd;
    margin-left: 85%;
    height: 35px;
    width: 155px;
    border: 1px solid #ddd;
    border-radius: 2px;
  }
</style>

<body class="sb-nav-fixed">
  <div id="layoutSidenav">
    <div id="layoutSidenav_content">
      <main>
        <div class="container-fluid">
          <h1 class="mt-4">Quản lý Khách hàng</h1>
          <div class="card mb-4">

          </div>
          <div class="card mb-4">
            <div class="card-header">
              <i class="fas fa-table mr-1"></i>
              DataTable
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <?php

                  // KẾT NỐI CSDL
                  include('./controller/connect.php');
                  // TÌM TỔNG SỐ RECORDS
                  $result = mysqli_query($conn, 'select count(idCustomer) as total from customer');
                  $row = mysqli_fetch_array($result);
                  $total_records = $row['total'];
                  // echo "<pre/>";
                  // var_dump($total_records);

                  // TÌM LIMIT VÀ CURRENT_PAGE
                  $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                  $limit = 4;

                  //  TÍNH TOÁN TOTAL_PAGE VÀ START
                  // tổng số trang
                  $total_page = ceil($total_records / $limit);

                  // Giới hạn current_page trong khoảng 1 đến total_page
                  if ($current_page > $total_page) {
                    $current_page = $total_page;
                  } else if ($current_page < 1) {
                    $current_page = 1;
                  }

                  // Tìm Start
                  $start = ($current_page - 1) * $limit;
                  //     echo "<pre/>";
                  // var_dump($result);
                  ?>
                  <?php
                  // TRUY VẤN LẤY DANH SÁCH 
                  // Có limit và start rồi thì truy vấn CSDL lấy danh sách gundam
                  include("./controller/connect.php");
                  $sql = "SELECT * FROM customer  LIMIT $start, $limit";
                  $query = mysqli_query($conn, $sql);
                  ?>
                  <?php
                  include('./controller/connect.php');
                  $output = "";
                  if (isset($_POST['btnSearch'])) {
                    $search = $_POST['txtSearch'];
                    if ($search != "") {
                      $query = mysqli_query($conn, "SELECT *FROM customer WHERE  idCustomer LIKE '%$search%' OR nameCustomer LIKE '%$search%'") or die("Lỗi truy vấn");
                      $result = mysqli_num_rows($query);
                      if ($result == 0) {
                        //Kết quả tìm thấy =0
                        $output .= "<span style='color:red;'>Không tìm thấy thông tin khách hàng '$search'</span>";
                      } else {
                        //Kết quả tìm kiếm >0
                        $output .= "<span style='color:#555;'>Có $result kết quả được tìm thấy với từ khóa'$search'</span>" . "<br>";
                        while ($row = @mysqli_fetch_assoc($query)) {

                          echo '<tr>';
                          echo "<td>{$row['idCustomer']}</td>";
                          echo "<td>{$row['nameCustomer']}</td>";
                          echo "<td>{$row['emailCustomer']}</td>";
                          echo "<td>{$row['phoneCustomer']}</td>";
                          echo "<td>{$row['addressCustomer']}</td>";
                          echo "<td>{$row['totalBill']}</td>";
                          echo "<td>{$row['typeCustomer']}</td>";
                  ?>
                          <td>
                            <a href="./view/form_update_customer.php?idCustomer=<?php echo $row['idCustomer']; ?>">Cập nhật</a>
                            <a href="../controller/delete_customer.php?idCustomer=<?php echo $id; ?>" onclick="return confirm('Bạn muốn xóa khách hàng này?');">Xoá</a>
                          </td>
                  <?php
                          echo '</tr>';
                        }
                      }
                    } else {
                      //Khong có ký tự nào ddc nhập
                      $output .= "<span style='color:red;'>Vui lòng nhập vào mã hoặc tên khách hàng cần tìm!</span>";
                    }
                  }
                  ?>
                  <form action="" method="POST">
                    <input type="text" name="txtSearch" placeholder="Nhập mã hoặc tên khách hàng" />
                    <input type="submit" name="btnSearch" value="Search" />
                    <button type="submit"><a href="./index.php?pid=6">Thêm Khách Hàng</a></button>
                  </form>
                  <?php
                  echo $output;
                  ?>
                  <thead>
                    <tr>
                      <th>Mã Khách hàng</th>
                      <th>Tên Khách hàng</th>
                      <th>Email</th>
                      <th>Số điện thoại</th>
                      <th>Địa chỉ</th>
                      <th>Tổng số hoá đơn</th>
                      <th>Loại khách hàng</th>
                      <th>Tuỳ chọn</th>
                    </tr>
                  </thead>
                  <?php
                  while ($data = mysqli_fetch_array($query)) {
                    $i = 1;
                    $id = $data['idCustomer'];
                  ?>
                    <tr>
                    <tr>
                      <td><?php echo $data['idCustomer'];  ?></td>
                      <td><?php echo $data['nameCustomer'];  ?></td>
                      <td><?php echo $data['emailCustomer'];  ?></td>
                      <td><?php echo $data['phoneCustomer'];  ?></td>
                      <td><?php echo $data['addressCustomer'];  ?></td>
                      <td><?php echo $data['totalBill'];  ?></td>
                      <td><?php if ($data['totalBill'] <= "5") {
                            echo "Khách hàng";
                          } elseif ($data['totalBill'] <= "15") {
                            echo " Khách hàng thân thiết";
                          } elseif ($data['totalBill'] <= "50") {
                            echo " Khách hàng bạc";
                          } elseif ($data['totalBill'] <= "150") {
                            echo " Khách hàng vàng";
                          } elseif ($data['totalBill'] <= "300") {
                            echo " Khách kim cương";
                          }
                          ?></td>
                      <td>
                        <a href="./view/form_update_customer.php?idCustomer=<?php echo $id; ?>">Cập nhật
                        </a>

                        <a href="./controller/delete_customer.php?idCustomer=<?php echo $id; ?>" onclick="return confirm('Bạn muốn xóa khách hàng này?');">Xoá

                        </a>
                      </td>
                    </tr>

                    </tr>
                  <?php
                    $i++;
                  }
                  ?>


                </table>
                <?php
                // PHẦN HIỂN THỊ PHÂN TRANG
                // nếu current_page > 1 và total_page > 1 mới hiển thị nút Trở về
                if ($current_page > 1 && $total_page > 1) {
                  echo '<a href="index.php?pid=3&&page=' . ($current_page - 1) . '">Trở về</a> ';
                }

                // Lặp khoảng giữa
                for ($i = 1; $i <= $total_page; $i++) {
                  // Nếu là trang hiện tại thì hiển thị thẻ span
                  // ngược lại hiển thị thẻ a
                  if ($i == $current_page) {
                    echo '<button><span>' . $i . '</span></button> ';
                  } else {
                    echo '<button><a href="index.php?pid=3&&page=' . $i . '">' . $i . '</a></button> ';
                  }
                }
                // nếu current_page < $total_page và total_page > 1 mới hiển thị nút trang tiếp theo
                if ($current_page < $total_page && $total_page > 1) {
                  echo '<a href="index.php?pid=3&&page=' . ($current_page + 1) . '">Trang tiếp theo</a> ';
                }
                ?>
              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  </div>
</body>
<!-- http://localhost:8080/bt/Learn/btl_web/admin/view/form_update_customer.php?idCustomer=2 -->
<!-- http://localhost:8080/bt/Learn/btl_web/admin/form_update_customer.php?idCustomer=3 -->
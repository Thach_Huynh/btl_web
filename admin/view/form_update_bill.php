<body>

   <meta http-equiv="Content-Type" content="text/html"; charset="UTF-8"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
        <title></title>
        <style>
            form{
                font-family: 'Times New Roman', Times, serif;
                /* background-color: whitesmoke;*/
                border-collapse: collapse;
                width: 50%;
                border: 1px solid #000;
                margin-left: 25%;
                background-color: whitesmoke;
                margin-top: 150px;
            }

            h2 {
                color: #17a2b8;
                padding-left: 20px;
            }

            input[type="text"] {
                width: 400px;
                height: 30px;

            }

            button[type="submit"] {
                font-family: 'Times New Roman', Times, serif;
                background-color: seagreen;
                margin-left: 40%;
                width: 100px;
                height: 40px;
                border: 1px solid #ddd;
                border-radius: 2px;
                margin-bottom: 20px;
                margin-top: 20px;
            }

            th,
            td {
                padding-left: 50px;
            }

            select {
                font-family: 'Times New Roman', Times, serif;
                width: 100%;
                height: 30px;
            }
        </style>
    </head>
</body>
<?php
     header('Content-Type: text/html; charset=UTF-8');
     $id = $_GET['idBill'];
     //Kết nối đến sever
     include('../controller/connect.php');
     $bill=("select *from bill where idBill='$id'") or die("Lỗi truy vấn");
     $query=mysqli_query($conn,$bill);
     $rs=mysqli_fetch_array($query);
   
?>
<form action="../controller/update_bill.php" method="POST">
    <h2>Cập nhật hoá đơn</h2>
    <table>
    <input type="hidden" name="idBill" value="<?php echo ($id); ?>">
    <tr>
            <th>Tên người nhận</th>
            <td><input type="text" name="name" value="<?php echo $rs['name']; ?>"></td>
        </tr>
        <tr>
            <th>Số điện thoại</th>
            <td><input type="text" name="phone" value="<?php echo $rs['phone']; ?>"></td>
        </tr>
       <tr>
            <th>Email</th>
            <td><input type="text" name="email" value="<?php echo $rs['email']; ?>"></td>
        </tr>
        <tr>
            <th>Địa chỉ</th>
            <td><input type="text" name="address" value="<?php echo $rs['address']; ?>"></td>
        </tr>
    <tr>
            <th>Trạng thái đơn:</th>
            <td>
    <select class="select_branch" name="status" >
      <option value="0">Chưa xác nhận</option>
      <option value="1">Đã xác nhận</option>
    </select>
        </td>
        </tr>
        <tr>
            <th>Thanh toán:</th>
            <td>
    <select class="select_branch" name="payment_status" >
      <option value="0">Chưa thanh toán</option>
      <option value="1">Đã thanh toán</option>
    </select>
        </td>
        </tr>
    </table>
    <button type="submit"><b>Update</b></button>
</form>
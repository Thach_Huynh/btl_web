<?php
session_start();
if (!isset($_SESSION["login"])) {
	header("location:view/login_form.php");
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<title>Hệ thống quản lý cửa hàng</title>
	<meta charset="utf-8" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
	<meta name="description" content="" />
	<meta name="author" content="" />
	<title>Dashboard - SB Admin</title>
	<link href="modal/css/styles.css" rel="stylesheet" />
	<link rel="shortcut icon" href="../Homepage/images/favicon.ico" type="image/x-icon">
	<link href="https://cdn.datatables.net/1.10.20/css/dataTables.bootstrap4.min.css" rel="stylesheet" crossorigin="anonymous" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.1/js/all.min.js" crossorigin="anonymous"></script>
	<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
	<script src="modal/js/scripts.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
	<script src="modal/assets/demo/chart-area-demo.js"></script>
	<script src="modal/assets/demo/chart-bar-demo.js"></script>
	<script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
	<script src="modal/assets/demo/datatables-demo.js"></script>
</head>

<body class="sb-nav-fixed">
	<?php
	include 'view/header.php'
	?>
	<?php
	if (!isset($_GET['pid'])) {
		include 'view/menu_tab.php ';
		include 'view/home.php';
	} else {
		$pid = intval($_GET['pid']);
		include 'view/menu_tab.php ';
		switch ($pid) {
			case '0':
				include 'view/home.php';
				break;
			case '1':
				include 'view/bill.php';
				break;
			case '2':
				include 'view/menu.php';
				break;
			case '3':
				include 'view/customer.php';
				break;
			case '4':
				include 'view/form_add_menu.php';
				break;
			case '5':
				include 'view/bill_detail.php';
				break;
			case '6':
				include 'view/form_add_Customer.php';
				break;
			case '7':
				include 'view/type/HG.php';
				break;
			case '8':
				include 'view/type/RG.php';
				break;
			case '9':
				include 'view/type/MG.php';
				break;
			case '10':
				include 'view/type/PG.php';
				break;
			case '11':
				include 'view/type/SD.php';
				break;
			case '14':
				include 'view/employees.php';
				break;
			case '15':
				include 'view/form_add_employees.php';
				break;
			case '16':
				include 'view/revenue.php';
				break;
		}
	}
	?>
	</div>
</body>

</html>
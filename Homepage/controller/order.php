<?php  if (!isset($_SESSION["login_home"])) {
		   echo '<script language="javascript">alert("Vui lòng đăng nhập để tiên hành mua sản phẩm"); window.location="./view/login_form.php";</script>';
    }?>
    <?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    ?>
	
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./modal/css/cart.css" >
    </head>
    <body>
        <?php
        include('connect.php');
        if (!isset($_SESSION["cart"])) {
            $_SESSION["cart"] = array();
        }
        $error = false;
        $success = false;
        if (isset($_GET['action'])) {
            switch ($_GET['action']) 
            {
                case "submit":
                  if ($_POST['order_click'])
                     { //Đặt hàng sản phẩm
                       
                        if ($error == false && !empty($_POST['quantity'])) { //Xử lý lưu giỏ hàng vào db
                            $products = mysqli_query($conn, "SELECT * FROM `menu` WHERE `idMenu` IN (" . implode(",", array_keys($_POST['quantity'])) . ")");                 
                            $total = 0;
                            $orderProducts = array();
                            while ($row = mysqli_fetch_array($products))  {
                                $orderProducts[] = $row;
                                $total += $row['priceMenu'] * $_POST['quantity'][$row['idMenu']];
                            }
                            $timeorder=date('Y-m-d H:i:s');
                            $insertOrder = mysqli_query($conn, "INSERT INTO `bill` (`idBill`,`idCustomer`,`name`,`phone`,`email`,`address`,`dateBill`, `Note`, `total`,`payment`) VALUES (NULL, '" .$_POST['idCustomer'] . "','" .$_POST['name']. "', '" .$_POST['phone']. "', '" .$_POST['email'] . "', '" .$_POST['address'] . "','" . $timeorder . "', '" . $_POST['Note'] . "', '" . $total . "','" .$_POST['payment']. "')");
                           
                            $orderID = $conn->insert_id;
                            $insertString = "";
                            foreach ($orderProducts as $key => $product) {
                                $insertString .= "(NULL, '" . $orderID . "',  '" . $product['idMenu'] . "','". $_POST['quantity'][$product['idMenu']] ."', '" . $product['priceMenu'] . "')";
                                if ($key != count($orderProducts) - 1) {
                                    $insertString .= ",";
                                }
                            }
                            $insertOrder = mysqli_query($conn,"INSERT INTO `bill_detail` (`id`, `idBill`,`idMenu`, `quantity`,`price`) VALUES " .$insertString. ";");
                            $success = '<script language="javascript">alert("Đặt hàng thành công");window.location="./index.php?pid=2"</script>';
                            unset($_SESSION['cart']);
                        }
                    }
                    break;
            }
        }
        if (!empty($_SESSION["cart"])) {
            $products = mysqli_query($conn, "SELECT * FROM `menu` WHERE `idMenu` IN (" . implode(",", array_keys($_SESSION["cart"])) . ")");
        }
        ?>
        <div class="container">
            <?php if (!empty($error)) { ?> 
                <div id="notify-msg">
                    <?= $error ?>. <a href="javascript:history.back()">Quay lại</a>
                </div>
            <?php } elseif (!empty($success)) { ?>
                <div id="notify-msg">
                    <?= $success ?>. <a href="javascript:index.php">Tiếp tục mua hàng</a>
                </div>
            <?php } else { ?>
                <h1>Tiến hành đặt hàng và thanh toán</h1>
                <form id="cart-form" action="./index.php?pid=12&action=submit" method="POST">
                    <table>
                        <tr>
                            <th class="product-number">STT</th>
                            <th class="product-name">Tên sản phẩm</th>
                            <th class="product-img">Ảnh sản phẩm</th>
                            <th class="product-price">Đơn giá</th>
                            <th class="product-quantity">Số lượng</th>
                            <th class="total-money">Thành tiền</th>
                        
                        </tr>
                        <?php
                        if (!empty($products)) {
                            $total = 0;
                            $num = 1;
                            while ($row = mysqli_fetch_array($products)) {
                                ?>
                                <tr>
                                    <td class="product-number"><?= $num; ?></td>
                                    <td class="product-name"><?= $row['nameMenu'] ?></td>
                                    <td class="product-img"><img src="images/<?= $row['imageMenu'] ?>" /></td>
                                    <td class="product-price"><?= number_format($row['priceMenu'], 0, ",", ".") ?>VNĐ</td>
                                    <td class="product-quantity"><input type="text" value="<?= $_SESSION["cart"][$row['idMenu']] ?>" name="quantity[<?= $row['idMenu'] ?>]" /></td>
                                    <td class="total-money"><?= number_format($row['priceMenu'] * $_SESSION["cart"][$row['idMenu']], 0, ",", ".") ?>VNĐ</td>
                                </tr>
                                <?php
                                $total += $row['priceMenu'] * $_SESSION["cart"][$row['idMenu']];
                                $num++;
                            }
                            ?>
                            <tr id="row-total">
                                <td class="product-number">&nbsp;</td>
                                <td class="product-name">Tổng tiền</td>
                                <td class="product-img">&nbsp;</td>
                                <td class="product-price">&nbsp;</td>
                                <td class="product-quantity">&nbsp;</td>
                                <td class="total-money"><?= number_format($total, 0, ",", ".") ?>VNĐ</td>
                                
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <form id="cart-form" action="./index.php?pid=12&action=submit" method="POST">
                    <hr>
                    <div><input type="hidden" value="<?php echo $_SESSION["login_home"][0] ?>" name="idCustomer"  /></div>
                    <div><label>Họ tên: </label><input type="text" name="name"/></div>
                    <div><label>Điện thoại: </label><input type="text" name="phone"  /></div>
                    <div><label>Email:</label><input type="text" name="email"/></div>
                    <div><label>Địa chỉ: </label><input type="text" name="address"/></div>
                    <div><label>Ghi chú: </label><textarea name="Note" cols="50" rows="7" ></textarea></div>
                   
   
    <label>Phương thức thanh toán: </label>
    <select class="select" name="payment" >
      <option value="0">Thanh toán khi nhận hàng</option>
      <option value="1">Thanh toán qua thẻ</option>
      <option value="2">Thanh toán qua ví điện tử</option>
    </select>
    
     
                    <div><input type="submit" name="order_click" value="Thanh toán" onclick="return confirm('Bạn muốn đặt món này món này?');" /></div>
                </form>
            <?php } ?>
        </div>
    </body>
</html>
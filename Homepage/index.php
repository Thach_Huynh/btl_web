<?php session_start(); ?>
<!DOCTYPE html>
<html class=no-js>

<head>
    <meta charset=utf-8>
    <title>Welcome to Gundam shop | Homepage</title>
    <meta name=description content="">
    <meta name=viewport content="width=device-width,initial-scale=1">
    <link rel=apple-touch-icon href=images/apple-touch-icon.png>
    <link rel="shortcut icon" href="./images/favicon.ico" type="image/x-icon">
    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
    <link rel=stylesheet href=modal/styles/vendor.css>
    <link rel=stylesheet href=modal/styles/main.css>
    <link rel=stylesheet href=modal/css/style1.css>
    <link rel=stylesheet href=modal/css/cart.css>
    <script src=modal/scripts/vendor/modernizr.js></script>
    <script src=modal/scripts/vendor.js></script>
    <script src=modal/scripts/plugins.js></script>
    <script src=modal/scripts/main.js></script>
    <link rel="stylesheet" href="modal/table_css/all.css">
    <script src="./modal/jquery-3.3.1.min.js"></script>
    <script src="./modal/popper.min.js"></script>
    <script src="./modal/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./modal/style.css">
    <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">

</head>

<body class=home-page>
    <?php
    include 'view/header.php ';
    ?>
    <?php
    if (!isset($_GET['pid'])) {
        include 'view/topgundam.php';
        include 'view/menu.php ';
        include 'view/review.php';
        include 'view/contact.php';
        include 'view/footer.php';
    } else {
        $pid = intval($_GET['pid']);

        switch ($pid) {
            case '1':
                include 'view/menu.php';
                break;
            case '2':
                include 'view/bill.php';
                break;
            case '3':
                include 'view/cart.php';
                break;
            case '4':
                include 'view/contact.php';
                break;
            case '5':
                include 'view/type/HG.php';
                break;
            case '6':
                include 'view/type/RG.php';
                break;
            case '7':
                include 'view/type/MG.php';
                break;
            case '8':
                include 'view/type/PG.php';
                break;
            case '9':
                include 'view/type/SD.php';
                break;
            case '10':
                include 'view/profile.php';
                break;
            case '11':
                include 'view/bill_detail.php';
                break;
            case '12':
                include 'controller/order.php';
                break;
            case '13':
                include 'view/detail_menu.php';
                break;
            case '14':
        }
        include("view/footer.php");
    }
    ?>
<?php if (!isset($_SESSION["login_home"])) {
  echo '<script language="javascript">alert("Vui lòng đăng nhập để xem hoá đơn"); window.location="./view/login_form.php";</script>';
} ?>
<style>
  button[type="submit"] {
    background-color: #ddd;
    margin-left: 85%;
    height: 35px;
    width: 155px;
    border: 1px solid #ddd;
    border-radius: 2px;
  }

  input[name="cofirm_ship"] {
    background-color: black;
    margin-top: 3%;
    height: 35px;
    width: 145px;
    border: 1px solid #ddd;
    border-radius: 2px;
    color: white;
  }

  input[name="cofirm_bill"] {
    background-color: black;
    margin-top: 3%;
    height: 35px;
    width: 145px;
    border: 1px solid #ddd;
    border-radius: 2px;
    color: white;
  }
</style>

<body class="sb-nav-fixed">
  <div id="layoutSidenav">
    <div id="layoutSidenav_content">
      <main>
        <div class="container-fluid">
          <h1 class="mt-4">Hoá đơn của bạn</h1>
          <div class="card mb-4">
          </div>
          <div class="card mb-4">

            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                  <!-- Phân trang -->
                  <?php
                  // ($_SESSION["login_home"][0]); == idCustomer
                  // KẾT NỐI CSDL
                  include('./controller/connect.php');
                  // TÌM TỔNG SỐ RECORDS
                  $idCus = ($_SESSION["login_home"][0]);
                  $result = mysqli_query($conn, "select count(idCustomer) as total from bill where idCustomer like '%$idCus'");
                  $row = mysqli_fetch_array($result);
                  $total_records = $row['total'];
                  // echo "<pre/>";
                  // var_dump($total_records);
                  // TÌM LIMIT VÀ CURRENT_PAGE
                  $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
                  $limit = 10;
                  //  TÍNH TOÁN TOTAL_PAGE VÀ START
                  // tổng số trang
                  $total_page = ceil($total_records / $limit);
                  // Giới hạn current_page trong khoảng 1 đến total_page
                  if ($current_page > $total_page) {
                    $current_page = $total_page;
                  } else if ($current_page < 1) {
                    $current_page = 1;
                  }
                  // Tìm Start
                  $start = ($current_page - 1) * $limit;
                  //     echo "<pre/>";
                  // var_dump($result);
                  ?>
                  <!-- Lấy db -->
                  <?php
                  // TRUY VẤN LẤY DANH SÁCH 
                  // Có limit và start rồi thì truy vấn CSDL lấy danh sách món ăn
                  include("./controller/connect.php");
                  $sql = "SELECT * FROM bill WHERE idCustomer = '$idCus' LIMIT $start, $limit";
                  $query = mysqli_query($conn, $sql);
                  ?>
                  <thead>
                    <tr>
                      <th>Tên người nhận</th>
                      <th>Số điện thoại</th>
                      <th>Địa chỉ</th>
                      <th>Email</th>
                      <th>Ngày lập HĐ</th>
                      <th>Chi tiết hóa đơn</th>
                      <th>Tổng tiền</th>
                      <th>Ghi chú</th>
                      <th>Trạng thái đơn hàng</th>
                      <th>Phương thức thanh toán</th>
                      <th>Trạng thái thanh toán</th>
                      <th>Tuỳ chọn</th>
                    </tr>
                  </thead>
                  <?php
                  while ($data = mysqli_fetch_array($query)) {
                    $i = 1;
                    $idCus = $data['idCustomer'];
                    $id = $data['idBill'];
                  ?>
                    <tr>
                      <td><?php echo $data['name'];  ?></td>
                      <td><?php echo $data['phone'];  ?></td>
                      <td><?php echo $data['address'];  ?></td>
                      <td><?php echo $data['email'];  ?></td>
                      <td><?php echo $data['dateBill'];  ?></td>
                      <td><a href="./index.php?pid=11&&idBill=<?php echo $id; ?>">Xem chi tiết đơn hàng</a></td>
                      <td><?php echo number_format($data['total'], 0, ',', '.');  ?>đ</td>
                      <td><?php echo $data['Note'];  ?></td>
                      <td><?php if ($data['status'] == "0") {
                            echo "Chưa xác nhận";
                          } elseif ($data['status'] == "1") {
                            echo " Đã xác nhận";
                          }
                          ?></td>
                      <td><?php if ($data['payment'] == "0") {
                            echo "Thanh toán khi nhận hàng";
                          } elseif ($data['payment'] == "1") {
                            echo "Thanh toán bằng thẻ";
                          } else {
                            echo "Thanh toán ví điện tử";
                          }
                          ?></b></td>
                      </td>
                      <td><?php if ($data['payment_status'] == "0") {
                            echo "Chưa thanh toán";
                          } elseif ($data['payment_status'] == "1") {
                            echo " Đã thanh toán";
                          }
                          ?></b></td>
                      <td><a href="./controller/cancel_bill.php?idBill=<?php echo $id; ?>&&status=<?php echo $data["status"] ?>" onclick="return confirm('Bạn muốn huỷ hoá đơn này?')">Huỷ hoá đơn</a></td>
                    </tr>
                  <?php
                    $i++;
                  }
                  ?>

                </table>
                <?php
                // PHẦN HIỂN THỊ PHÂN TRANG
                // nếu current_page > 1 và total_page > 1 mới hiển thị nút Trở về
                if ($current_page > 1 && $total_page > 1) {
                  echo '<a href="index.php?pid=2&&page=' . ($current_page - 1) . '">Trở về</a> ';
                }

                // Lặp khoảng giữa
                for ($i = 1; $i <= $total_page; $i++) {
                  // Nếu là trang hiện tại thì hiển thị thẻ span
                  // ngược lại hiển thị thẻ  a
                  if ($i == $current_page) {
                    echo '<button><span>' . $i . '</span></button> ';
                  } else {
                    echo '<button><a href="index.php?pid=2&&page=' . $i . '">' . $i . '</a></button> ';
                  }
                }
                // nếu current_page < $total_page và total_page > 1 mới hiển thị nút trang tiếp theo
                if ($current_page < $total_page && $total_page > 1) {
                  echo '<a href="index.php?pid=2&&page=' . ($current_page + 1) . '">Trang tiếp theo</a> ';
                }
                ?>

              </div>
            </div>
          </div>
        </div>
      </main>
    </div>
  </div>
</body>
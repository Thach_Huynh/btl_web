<?php
include('./controller/connect.php');
?>
<section class=events-slide>
    <div class=container>
        <div>
            <div class=section-number><span>01</span></div>
            <div class=section-heading>
                <h2>Top Gundam</h2>
            </div>
        </div>
        <div class=event-container id=event-owl>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/ev1.png alt=""> </div>
                <div class=event-desc>
                    <h3>GUNDAM BARBATOS - CÔNG TƯỚC ĐỊA NGỤC</h3>
                    <p>Từ bỏ những thiết kế vuông vức quen thuộc thường gặp ở các thế hệ Gundam trước kia, Barbatos đến từ series Iron-Blooded Orphans mang đến làn gió mới mẻ bằng cách thay đổi những đường gốc vuông vức thành các đường bo tròn, mượt mà hơn. Xét về ngoại hình, Barbatos sở hữu những đặc trưng giống một con quỷ với bộ móng guốc to ở chân giống với loài dê, móng vuốt chực chờ xé xác kẻ thù đầy hoang dã, màu sắc chủ đạo là trắng, đỏ, xanh, tô điểm nhấn bằng màu vàng nổi bật.</p>
                    <p>Cũng giống với tên của mình Barbatos đã thể hiện được vẻ ngoài trông giống như một Đại công tước đến từ Địa ngục: cặp sừng vàng nhấn mạnh mẽ, phần cằm đỏ thiết kế nhô lởm chởm trông như một bộ râu quý phái, tạo cho người nhìn ấn tượng sâu đậm về Ác quỷ trắng, về một trong những Gundam đẹp nhất!</p>
                    <a href="./index.php?pid=13&&idMenu=11">Mua ngay</a>
                </div>
            </div>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/rg3.png alt=""> </div>
                <div class=event-desc>
                    <h3>RX-78-2 - GUNDAM ĐẸP NHẤT VÀ ĐẶC BIỆT NHẤT</h3>
                    <p>Là khởi nguồn của cả thương hiệu Gundam hiện thời, RX-78-2 hay còn có cái tên 'Cụ tổ' thân thương chính là mẫu Mobile Suit mà bất cứ người chơi Gunpla hoặc yêu thích series hoạt hình Gundam đều mê mẩn. Trải qua hơn 40 năm với nhiều thay đổi, RX-78-2 Gundam liên tục được làm mới, tinh chỉnh lại thiết kế ở mỗi lần ra mắt. Để đến ngày này, với những tiêu chuẩn hiện đại, nó vẫn không hề lỗi thời, đó là một sự bền bỉ không tưởng.</p>
                    <p>RX-78-2 Gundam không có vũ khí khủng, không có cánh lớn, không có những trang bị hỗ trợ hầm hố, nhưng nó lại có thiết kế cân đối đến đáng ngạc nhiên, với 4 màu đỏ, xanh, vàng trắng quen thuộc. Các đường nét giáp ngoài tuy đơn giản nhưng tinh tế, gần như không có chi tiết thừa. Đó là thành quả của một quá trình dài cải tiến. Với xu hướng tối giản đang lên ngôi hiện nay, RX-78-2 Gundam sẽ còn hợp thời rất rất lâu, nếu bạn chưa sở hữu thì nên sắm ngay đi nào!</p>
                    <a href="./index.php?pid=13&&idMenu=16">Mua ngay</a>
                </div>
            </div>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/rg2.png alt=""> </div>
                <div class=event-desc>
                    <h3>GUNDAM EXIA & GUNDAM 00</h3>
                    <p>Đã hơn 10 năm trôi qua kể từ ngày đầu tiên phát hành series Mobile Suit Gundam 00 nhưng cho đến tận nay, series này vẫn không hề có dấu hiệu hạ nhiệt! Những Mobile Suit xuất hiện trong series này luôn luôn nằm trong top Gundam đẹp nhất bởi thiết kế vô cùng mới mẻ đặc trưng. Gundam Exia và 00 Gundam có rất nhiều biến thể khác như Gundam Avalanche Exia, Exia Repair (I, II, III, IV), 00 Seven Sword, 00 Raiser... với số lượng kit cực kì khủng.</p>
                    <p>Ngoài ra, những biến thể của Exia và 00 còn được phát hành nhiều mẫu Gunpla khác nhau, thay đổi chút ít ở mỗi lần ra mắt, một điều mà không mẫu Mobile Suit nào trong cùng series làm được. Đến tận bây giờ, thiết kế của Gundam Exia và 00 Gundam vẫn được cải biên lại và tiếp tục ra mới. Như trong phim Gundam Build Fighters chúng ta có Gundam Amazing Exia, Gundam Exia Dark Matter. Còn trong Gundam Build Divers, nhân vật chính đã dùng hẳn Gundam 00 Diver kế thừa thiết kế từ Gundam 00.</p>
                    <a href="./index.php?pid=13&&idMenu=18">Mua ngay</a>
                </div>
            </div>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/hg2.png alt=""> </div>
                <div class=event-desc>
                    <h3>FREEDOM GUNDAM, DESTINY GUNDAM & FORCE IMPULSE GUNDAM</h3>
                    <p>Mặc kệ việc series phim Gundam SEED và SEED Destiny gây ra nhiều vụ tranh cãi giữa các fan, bộ 3 Freedom Gundam, Destiny Gundam & Force Impulse Gundam vẫn sẽ là một trong những Gundam đẹp nhất mà bất cứ ai cũng phải thừa nhận.</p>
                    <p>Freedom Gundam, Freedom Strike Gundam - Đôi cánh tự do của bầu trời, sở hữu đôi cánh lớn chứa đựng vũ khí Super Dragoon với những chi tiết màu ánh kim vàng cực đẹp độc đáo khi dang rộng.</p>
                    <p>Destiny Gundam - Đôi cánh ánh sáng Wings of Light là một trong những điểm nổi bật nhất của Destiny. Với thiết kế hơi có phần giống cánh bướm nhưng màu hồng đỏ tuyệt đẹp đã góp phần giúp cho Destiny trở thành một trong những Gundam đẹp nhất.</p>
                    <a href="./index.php?pid=13&&idMenu=2">Mua ngay</a>
                </div>
            </div>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/mg1.png alt=""> </div>
                <div class=event-desc>
                    <h3>WING GUNDAM - GUNDAM ĐẸP NHẤT VỚI ĐÔI CÁNH THIÊN THẦN</h3>
                    <p>Là khởi nguồn của cả thương hiệu Gundam hiện thời, RX-78-2 hay còn có cái tên 'Cụ tổ' thân thương chính là mẫu Mobile Suit mà bất cứ người chơi Gunpla hoặc yêu thích series hoạt hình Gundam đều mê mẩn. Trải qua hơn 40 năm với nhiều thay đổi, RX-78-2 Gundam liên tục được làm mới, tinh chỉnh lại thiết kế ở mỗi lần ra mắt. Để đến ngày này, với những tiêu chuẩn hiện đại, nó vẫn không hề lỗi thời, đó là một sự bền bỉ không tưởng.</p>
                    <p>RX-78-2 Gundam không có vũ khí khủng, không có cánh lớn, không có những trang bị hỗ trợ hầm hố, nhưng nó lại có thiết kế cân đối đến đáng ngạc nhiên, với 4 màu đỏ, xanh, vàng trắng quen thuộc. Các đường nét giáp ngoài tuy đơn giản nhưng tinh tế, gần như không có chi tiết thừa. Đó là thành quả của một quá trình dài cải tiến. Với xu hướng tối giản đang lên ngôi hiện nay, RX-78-2 Gundam sẽ còn hợp thời rất rất lâu, nếu bạn chưa sở hữu thì nên sắm ngay đi nào!</p>
                    <a href="./index.php?pid=13&&idMenu=2">Mua ngay</a>
                </div>
            </div>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/sd1.png alt=""> </div>
                <div class=event-desc>
                    <h3>VALKYLANDER - EX VALKYLANDER</h3>
                    <p>Valkylander - Một trong những mẫu Gundam đẹp nhất thuộc dòng SD Gundam với khả năng biến hình đa dạng: từ Valkylander mode sang dạng rồng Gundragon Mode hoành tráng, hay chuyển thành Dragon Fusion Mode, bổ sung thêm đôi cánh phía sau ở dạng Valkylander. Vũ khí đi kèm GN Flare Device và GN Launcher Device cũng có thể kết hợp lại thành GN Mega Flare Device - khẩu súng lớn với sức công phá cực lớn.</p>
                    <p>Ngoài ra còn có EX Valkylander - Saint Metal Changer Ex Valkylander - Phiên bản nâng cấp của Valkylander ở trên bởi Parviz với màu trắng tuyệt đẹp, đặc biệt là Legend Wings - đôi cánh ở dạng Dragon Fusion mang nhiều cải tiến. Vũ khí đi kèm cũng nhiều hơn, nổi bật nhất là Twin Gunblade có thể kết hợp với Gunshield để tạo thành GN Ex Ballistar càng góp phần cho Valkylander trở thành một trong số Gundam đẹp nhất!</p>
                    <a href="./index.php?pid=13&&idMenu=2">Mua ngay</a>
                </div>
            </div>
            <div class="event-single clearfix"> <img class=star-mark src=images/star-fav.png alt="">
                <div class="imgLiquidFill imgLiquid event-img"> <img src=images/pg1.png alt=""> </div>
                <div class=event-desc>
                    <h3>UNICORN GUNDAM - MỘT TRONG NHỮNG GUNDAM MẠNH NHẤT</h3>
                    <p>Một trong số những Mobile Suit mạnh nhất và cũng là một trong những Gundam đẹp nhất: Unicorn Gundam. Khả năng tương thích với Pilot nên sức mạnh của Unicorn thì không cần phải bàn cãi. Khi chuyển sang Destroy Mode, Psycho-frame kết nối Pilot với Drive System giúp cho Pilot có thể điều khiển cả Mobile Suit này chỉ bằng suy nghĩ.</p>
                    <p>Destroy Mode cũng là trạng thái giúp cho Unicorn Gundam giành được vị trí trong top Gundam đẹp nhất này khi xen kẽ các bộ phận trắng phau là những Psycho-frame rực rỡ trong suốt tuyệt đẹp, bắt mắt mà bất cứ ai cũng sẽ cảm thấy cực kì ấn tượng. Chính vì điều này nên Bandai cũng 'chăm chỉ' đưa ra vô cùng nhiều phiên bản khác nhau của Unicorn như: Unicorn Banshee, Unicorn Phenex, Full Armor Unicorn, Knight Unicorn, Sangoku Liu Bei Unicorn Gundam v.v... và tất cả chúng đều xứng đáng là những Gundam đẹp hiện nay.</p>
                    <a href="./index.php?pid=13&&idMenu=2">Mua ngay</a>
                </div>
            </div>
        </div>
    </div>
</section>
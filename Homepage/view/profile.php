<style>
    body {
        background-color: #f9f9fa
    }

    .padding {
        padding: 3rem !important
    }

    .user-card-full {
        overflow: hidden
    }

    .card {
        border-radius: 5px;
        -webkit-box-shadow: 0 1px 20px 0 rgba(69, 90, 100, 0.08);
        box-shadow: 0 1px 20px 0 rgba(69, 90, 100, 0.08);
        border: none;
        margin-bottom: 30px
    }

    .m-r-0 {
        margin-right: 0px
    }

    .m-l-0 {
        margin-left: 0px
    }

    .user-card-full .user-profile {
        border-radius: 5px 0 0 5px
    }

    .bg-c-lite-green {
        background: -webkit-gradient(linear, left top, right top, from(#f29263), to(#ee5a6f));
        background: linear-gradient(to right, #ee5a6f, #f29263)
    }

    .user-profile {
        padding: 20px 0
    }

    .card-block {
        padding: 1.25rem
    }

    .m-b-25 {
        margin-bottom: 25px
    }

    .img-radius {
        border-radius: 5px
    }

    h6 {
        font-size: 14px
    }

    .card .card-block p {
        line-height: 25px
    }

    @media only screen and (min-width: 1400px) {
        p {
            font-size: 14px
        }
    }

    .card-block {
        padding: 1.25rem
    }

    .b-b-default {
        border-bottom: 1px solid #e0e0e0
    }

    .m-b-20 {
        margin-bottom: 20px
    }

    .p-b-5 {
        padding-bottom: 5px !important
    }

    .card .card-block p {
        line-height: 25px
    }

    .m-b-10 {
        margin-bottom: 10px
    }

    .text-muted {
        color: #919aa3 !important
    }

    .b-b-default {
        border-bottom: 1px solid #e0e0e0
    }

    .f-w-600 {
        font-weight: 600
    }

    .m-b-20 {
        margin-bottom: 20px
    }

    .m-t-40 {
        margin-top: 20px
    }

    .p-b-5 {
        padding-bottom: 5px !important
    }

    .m-b-10 {
        margin-bottom: 10px
    }

    .m-t-40 {
        margin-top: 20px
    }

    .user-card-full .social-link li {
        display: inline-block
    }

    .user-card-full .social-link li a {
        font-size: 20px;
        margin: 0 10px 0 0;
        -webkit-transition: all 0.3s ease-in-out;
        transition: all 0.3s ease-in-out
    }

    button {
        font-family: 'Times New Roman';
        background-color: white;
        margin-left: 15%;
        width: 100px;
        height: 40px;
        border: 3px solid #ddd;
        border-radius: 4px;
        margin-bottom: 20px;
        margin-top: 20px;
    }
</style>

<div class="page-content page-container" id="page-content">
    <div class="padding">
        <div class="row container d-flex justify-content-center">
            <div class="col-xl-6 col-md-12">
                <div class="card user-card-full">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-sm-4 bg-c-lite-green user-profile">
                            <div class="card-block text-center text-white">
                                <div class="m-b-25"> <img src="https://img.icons8.com/bubbles/100/000000/user.png" class="img-radius" alt="User-Profile-Image"> </div>
                                <?php
                                header('Content-Type: text/html; charset=UTF-8');
                                $id = $_GET['idCustomer'];
                                //Kết nối đến sever
                                include('../controller/connect.php');
                                $customer = ("select *from customer where idCustomer='$id'") or die("Lỗi truy vấn");
                                $query = mysqli_query($conn, $customer);
                                $rs = mysqli_fetch_array($query);
                                ?>
                                <h6 class="f-w-600">Khách hàng:<?php echo $rs['username']; ?></h6>
                                <p>
                                    <b>Hạng Khách hàng:</b>
                                    <?php if ($rs['typeCustomer'] == "Khách hàng") {
                                        echo '<b style=color:blue> "Khách hàng"</b>';
                                    } elseif ($rs['typeCustomer'] == "Khách kim thân thiết") {
                                        echo " Khách kim thân thiết";
                                    } elseif ($rs['typeCustomer'] == "Khách hàng bạc") {
                                        echo '<b> " Khách hàng bạc" </b>';
                                    } elseif ($rs['typeCustomer'] == "Khách hàng vàng") {
                                        echo " Khách hàng vàng";
                                    } elseif ($rs['typeCustomer'] == "Khách kim cương") {
                                        echo " Khách kim cương";
                                    }
                                    ?>
                                </p> <i class=" mdi mdi-square-edit-outline feather icon-edit m-t-10 f-16"></i>
                            </div>
                        </div>
                        <div class="col-sm-8">
                            <div class="card-block">
                                <h6 class="m-b-20 p-b-5 b-b-default f-w-600">Thông tin cá nhân</h6>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">Họ tên</p>
                                        <h6 class="text-muted f-w-400"><?php echo $rs['nameCustomer'] ?></h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">Phone</p>
                                        <h6 class="text-muted f-w-400"><?php echo $rs['phoneCustomer'] ?></h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">Email</p>
                                        <h6 class="text-muted f-w-400"><?php echo $rs['emailCustomer'] ?></h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">Địa chỉ</p>
                                        <h6 class="text-muted f-w-400"><?php echo $rs['addressCustomer'] ?></h6>
                                    </div>
                                    <div class="col-sm-6">
                                        <p class="m-b-10 f-w-600">Tổng số hoá đơn đã mua:</p>
                                        <h6 class="text-muted f-w-400"><?php echo $rs['totalBill'] ?></h6>
                                    </div>
                                    <button><a href="form_update_profile.php?idCustomer=<?php echo $id; ?>">
                                            Update</a></button>
                                    <button><a href="../index.php">
                                            Về trang chủ</a></button>
                                </div>

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
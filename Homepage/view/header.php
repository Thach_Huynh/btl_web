<header>
    
    <div class=header-body>
        <nav class="navbar navbar-default">
            <div class=container-fluid>
                <div class=row>
                    <div class=navbar-header> <button type=button class="navbar-toggle collapsed" data-toggle=collapse data-target=#coffeeNavbarPrimary aria-expanded=false> <span class=sr-only>Toggle navigation</span> <span class=icon-bar></span> <span class=icon-bar></span> <span class=icon-bar></span> </button> </div> <a href=index.php class=header-logo>Coffee and You <img src=images/small-logo.png alt=""> </a>
                    <div class="collapse navbar-collapse" id=coffeeNavbarPrimary>
                        <ul class="nav navbar-nav navbar-right">
                            <li class=active><a href="./index.php">Trang chủ</a></li>
                            <li><a href="./index.php?pid=1">Menu</a></li>
                            <li><a href="./index.php?pid=2">Hoá đơn của bạn</a></li>
                            <li><a href="./index.php?pid=3"> Giỏ hàng <i class="fa fa-shopping-cart"></i></a></li>
                            <?php if (!isset($_SESSION["login_home"])) {
                                echo '<li><a href=./view/login_form.php>Đăng Nhập</a></li>
                                    <li><a href=./controller/register.php>Đăng ký</a></li>';
                            } else {
                                echo '
                                <div class="dropdown">
                                <li>
                                <a class="nut_dropdown">
                                <b>Khách hàng: ' . ($_SESSION["login_home"][3]) . '</b>
                                </a>
                                <div class="noidung_dropdown">
                                <a href="./view/profile.php?idCustomer=' . ($_SESSION["login_home"][0]) . '" >Trang cá nhân</a>
                                <a href="./controller/logout.php">Đăng xuất</a>
                                </div>
                                <li>
                                </div>';
                            }
                            ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </nav>
    </div>
    <script src="./modal/jquery-3.3.1.min.js"></script>
    <script src="./modal/popper.min.js"></script>
    <script src="./modal/bootstrap/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="./modal/styles/main.css">
    <link rel="stylesheet" type="text/css" href="./modal/style.css">
    <link rel="stylesheet" type="text/css" href="./bootstrap/css/bootstrap.min.css">
</header>
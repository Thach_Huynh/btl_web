    <?php
    date_default_timezone_set('Asia/Ho_Chi_Minh');
    ?>
	
<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./modal/css/cart.css" >
    </head>
    <body>
        <?php
        include './controller/connect.php';
        if (!isset($_SESSION["cart"])) {
            $_SESSION["cart"] = array();
        }
        $error = false;
        $success = false;
        if (isset($_GET['action'])) {
            function update_cart($add = false) {
                foreach ($_POST['quantity'] as $id => $quantity) {
                    if ($quantity <= 0) {
                        unset($_SESSION["cart"][$id]);
                    }

                    else {
                        if ($add) {
                            $_SESSION["cart"][$id] += $quantity;
                        } else {
                            $_SESSION["cart"][$id] = $quantity;
                        }
                    }
                }
            }
            switch ($_GET['action']) 
            {
                case "add":
                    update_cart(true);
                    echo "<script>alert('Sản Phẩm Đã Thêm Vào Giỏ Hàng Thành Công!');window.location='./index.php?pid=3';</script>";
                    break;
                case "delete":
                    if (isset($_GET['idMenu'])) {
                        unset($_SESSION["cart"][$_GET['idMenu']]);
                    }
                    echo "<script>history.back();</script>";
                    break;
                case "submit":
                    if (isset($_POST['update_click'])) { //Cập nhật số lượng sản phẩm
                        update_cart();
                        echo "<script>history.back();</script>";
                    }
                    if ($_POST['order_click'])
                     { //Đặt hàng sản phẩm
                          if (!isset($_SESSION["login_home"])) {
                            echo '<script language="javascript">alert("Vui lòng đăng nhập để tiên hành mua sản phẩm"); window.location="./view/login_form.php";</script>';
                     }
                        
                         if (empty($_POST['quantity'])) {
                            echo'<script language="javascript">alert("Giỏ hàng đang rỗng, hãy chọn món thêm vào giỏ ngay");  window.location="./index.php?pid=1";</script>';
                        }
                        else {
                         echo '<script language="javascript">alert("Đang chuyển sang trang tiến hành đặt hàng và thanh toán");  window.location="./index.php?pid=12";</script>';   
                        }
                       
            }
        }
        }
        if (!empty($_SESSION["cart"])) {
            $products = mysqli_query($conn, "SELECT * FROM `menu` WHERE `idMenu` IN (" . implode(",", array_keys($_SESSION["cart"])) . ")");
        }
        ?>
        <div class="container">
                <h1>Giỏ hàng</h1>
                <form id="cart-form" action="./index.php?pid=3&action=submit" method="POST">
                    <table>
                        <tr>
                            <th class="product-number">STT</th>
                            <th class="product-name">Tên sản phẩm</th>
                            <th class="product-img">Ảnh sản phẩm</th>
                            <th class="product-price">Đơn giá</th>
                            <th class="product-quantity">Số lượng</th>
                            <th class="total-money">Thành tiền</th>
                            <th class="product-delete">Xóa</th>
                        </tr>
                        <?php
                        if (!empty($products)) {
                            $total = 0;
                            $num = 1;
                            while ($row = mysqli_fetch_array($products)) {
                                ?>
                                <tr>
                                    <td class="product-number"><?= $num; ?></td>
                                    <td class="product-name"><?= $row['nameMenu'] ?></td>
                                    <td class="product-img"><img src="images/<?= $row['imageMenu'] ?>" /></td>
                                    <td class="product-price"><?= number_format($row['priceMenu'], 0, ",", ".") ?>VNĐ</td>
                                    <td class="product-quantity"><input type="number" min="1" value="<?= $_SESSION["cart"][$row['idMenu']] ?>" name="quantity[<?= $row['idMenu'] ?>]" /></td>
                                    <td class="total-money"><?= number_format($row['priceMenu'] * $_SESSION["cart"][$row['idMenu']], 0, ",", ".") ?>VNĐ</td>
                                    <td class="product-delete"><a href="./index.php?pid=3&action=delete&idMenu=<?= $row['idMenu'] ?>"onclick="return confirm('Bạn muốn xóa sản phẩm này khỏi giỏ hàng?');"><span class="glyphicon glyphicon-trash" style='font-size:25px;color:black'></span></a></td>
                                </tr>
                                <?php
                                $total += $row['priceMenu'] * $_SESSION["cart"][$row['idMenu']];
                                $num++;
                            }
                            ?>
                            <tr id="row-total">
                                <td class="product-number">&nbsp;</td>
                                <td class="product-name">Tổng tiền</td>
                                <td class="product-img">&nbsp;</td>
                                <td class="product-price">&nbsp;</td>
                                <td class="product-quantity">&nbsp;</td>
                                <td class="total-money"><?= number_format($total, 0, ",", ".") ?>VNĐ</td>
                                <td class="product-delete"></td>
                            </tr>
                            <?php
                        }
                        ?>
                    </table>
                    <div id="form-button">
                        <input type="submit" name="update_click" value="Cập nhật" />
                        <input type="submit"  name="order_click" value="Đặt hàng"/>
                    </div>
                    
                   
                
         
        </div>
    </body>
</html>
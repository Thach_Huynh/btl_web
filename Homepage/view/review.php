<section class=testimonial-sectn>
            <div class=container>
                <div>
                    <div class=section-number><span>03</span></div>
                    <div class="section-heading with-gray">
                        <h2>Visitors</h2>
                    </div>
                </div>
                <div class="row testimonial">
                    <ul class="clearfix testimonial-owl">
                        <li class="testimonial-item item clearfix">
                            <div class="imgLiquidFill imgLiquid"> <img src=images/testimonial/member1.jpg alt=""> </div>
                            <div class=name-text>
                                <h3>William</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, commodi labore veritatis quasi fugiat, officiis eligendi architecto molestias non soluta qui voluptate ex quam velit laboriosam esse fuga sequi. Tenetur.</p>
                            </div>
                        </li>
                        <li class="testimonial-item item clearfix">
                            <div class="imgLiquidFill imgLiquid"> <img src=images/testimonial/member02.jpg alt=image> </div>
                            <div class=name-text>
                                <h3>maria</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, commodi labore veritatis quasi fugiat, officiis eligendi architecto molestias non soluta qui voluptate ex quam velit laboriosam esse fuga sequi. Tenetur.</p>
                            </div>
                        </li>
                        <li class="testimonial-item item clearfix">
                            <div class="imgLiquidFill imgLiquid"> <img src=images/testimonial/memeber03.jpg alt=image> </div>
                            <div class=name-text>
                                <h3>jhon</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, commodi labore veritatis quasi fugiat, officiis eligendi architecto molestias non soluta qui voluptate ex quam velit laboriosam esse fuga sequi. Tenetur.</p>
                            </div>
                        </li>
                        <li class="testimonial-item item clearfix">
                            <div class="imgLiquidFill imgLiquid"> <img src=images/testimonial/member04.jpg alt=image> </div>
                            <div class=name-text>
                                <h3>david</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, commodi labore veritatis quasi fugiat, officiis eligendi architecto molestias non soluta qui voluptate ex quam velit laboriosam esse fuga sequi. Tenetur.</p>
                            </div>
                        </li>
                        <li class="testimonial-item item clearfix">
                            <div class="imgLiquidFill imgLiquid"> <img src=images/testimonial/member05.jpg alt=image> </div>
                            <div class=name-text>
                                <h3>maria</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, commodi labore veritatis quasi fugiat, officiis eligendi architecto molestias non soluta qui voluptate ex quam velit laboriosam esse fuga sequi. Tenetur.</p>
                            </div>
                        </li>
                        <li class="testimonial-item item clearfix">
                            <div class="imgLiquidFill imgLiquid"> <img src=images/testimonial/member06.jpg alt=image> </div>
                            <div class=name-text>
                                <h3>luiz</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Dolorem, commodi labore veritatis quasi fugiat, officiis eligendi architecto molestias non soluta qui voluptate ex quam velit laboriosam esse fuga sequi. Tenetur.</p>
                            </div>
                        </li>
                    </ul>
                </div>
                <!-- <figure class="testimonial-text clearfix"> <img src=images/testi-img.png alt="">
                    <figcaption>
                        <h3>Our happy gundam lover and their awesome comments</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Sunt porro aliquid laborum omnis labore sit, pariatur neque veritatis quaerat tempore mollitia, tempora placeat. Recusandae ullam assumenda ipsa, eum laboriosam eius.</p>
                    </figcaption>
                </figure> -->
            </div>
        </section>
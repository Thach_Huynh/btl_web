<style>
    button[type="submit-order"] {
        color: black;
        width: 125px;
        height: 45px;
        margin-top: 2px;
        margin-right: 6px;
        float: left;
        background-color: white;
    }
    .page_m{
        margin-left: 45%;
    }
</style>

<body>
    <?php

    // KẾT NỐI CSDL
    include('./controller/connect.php');

    // TÌM TỔNG SỐ RECORDS
    $result = mysqli_query($conn, 'select count(idMenu) as total from menu WHERE typeMenu="MG"');
    $row = mysqli_fetch_array($result);
    $total_records = $row['total'];
    // echo "<pre/>";
    // var_dump($total_records);

    // TÌM LIMIT VÀ TRANG HIỆN TẠI
    $current_page = isset($_GET['page']) ? $_GET['page'] : 1;
    $limit = 8;

    //  TÍNH TOÁN TỔNG SỐ TRANG VÀ START
    // tổng số trang
    $total_page = ceil($total_records / $limit);

    // Giới hạn current_page trong khoảng 1 đến total_page
    if ($current_page > $total_page) {
        $current_page = $total_page;
    } else if ($current_page < 1) {
        $current_page = 1;
    }

    // Tìm Start
    $start = ($current_page - 1) * $limit;

    // TRUY VẤN LẤY DANH SÁCH 
    // Có limit và start rồi thì truy vấn CSDL lấy danh sách gundam ăn

    ?>
    <?php
    include("./controller/connect.php");
    $sql = "SELECT * FROM menu WHERE typeMenu='MG' LIMIT $start, $limit";
    $query = mysqli_query($conn, $sql);
    ?>
    <?php
    include('./controller/connect.php');
    $output = "";
    if (isset($_POST['btnSearch'])) {
        $search = $_POST['textSearch'];
        if ($search != "") {
            $query = mysqli_query($conn, "SELECT *FROM menu WHERE nameMenu LIKE '%$search%' and typeMenu='MG'") or die("Không tìm thấy gundam");
            $result = mysqli_num_rows($query);
            if ($result == 0) {
                //Kết quả tìm thấy =0
                $output .= "<br><span style='color:red;'>Không tìm thấy thông tin gundam '$search'</span>";
              } else {
                //Kết quả tìm kiếm >0
                $output .= "<span style='color:#34c959;'><br>Có $result kết quả được tìm thấy với từ khóa'$search'</span>" . "<br>";
                while ($row = @mysqli_fetch_assoc($query)){

                    echo '<tr>';
                    echo "<td>{$row['nameMenu']}</td>";
                    echo "<td>{$row['priceMenu']}</td>";
                    echo "<td>{$row['imageMenu']}</td>";
                    echo "<td>{$row['noteMenu']}</td>";
                    echo '</tr>';
                }
            }
        } else {
            //Khong có ký tự nào ddc nhập
            $output .= "<span style='color:red;'>Vui lòng nhập vào mã hoặc tên gundam ăn cần tìm</span>";
        }
    }
    ?>
    <section class=online-store>
        <div class=container>
            <div>
                <div class=section-number><span>01</span></div>
                <div class=section-heading>
                    <h2>Menu</h2>
                </div>
            </div>
            <ul class="categories row">
                <li><button type="submit" onclick="location.href='index.php?pid=1'">All</button></li>
                <li><button type="submit" onclick="location.href='index.php?pid=5'">HIGH GRADE (HG)</button></li>
                <li><button type="submit" onclick="location.href='index.php?pid=6'">REAL GRADE (RG)</button></li>
                <li><button type="submit" onclick="location.href='index.php?pid=7'">MASTER GRADE (MG)</button></li>
                <li><button type="submit" onclick="location.href='index.php?pid=8'">PERFECT GRADE (PG)</button></li>
                <li><button type="submit" onclick="location.href='index.php?pid=9'">SUPER DEFORMED (SD)</button></li>
            </ul>
            <form action="" method="POST">
                <input type="text" name="textSearch" placeholder="Nhập mã hoặc tên gundam" style="padding: 10px;border-radius: 5px;border: 1px solid black;border-color: lightgray" />
                <input type="submit" name="btnSearch" value="Tìm kiếm" style="padding: 10px;border-radius: 5px;border: 1px solid black;border-color: lightgray; background-color:gold" />
            </form>
            <?php
            echo $output;
            ?>
            <div class="store-product-list row">
                <?php
                //Hiển thị sản phẩm
                while ($data = mysqli_fetch_array($query)) {
                    $i = 1;
                    $id = $data['idMenu'];
                ?>
                    <div class="store-product-wrapper grid-item type">
                        <div class=store-product>
                            <div class="imgLiquidFill imgLiquid item-image"> <img src="./images/<?php echo $data['imageMenu'];  ?>" width="100px" height="80px" alt=""> 
                            <a href="./index.php?pid=13&&idMenu=<?php echo $id; ?>"></a></div>
                            <div class=product-detail>
                                <div class=product-rate><?php echo number_format($data['priceMenu'], 0, ',', '.');  ?>đ</div>
                                <h3><?php echo $data['nameMenu'];  ?></h3>

                            </div>
                            <form id="add-to-cart-form" action="./index.php?pid=3&&action=add" method="POST">
                                <input type="number" min="1" value="1" name="quantity[<?= $data['idMenu'] ?>]" size="5" /><br />
                                <input type="submit" value="Thêm vào giỏ hàng" />
                            </form>
                        </div>
                    </div>
                <?php
                    $i++;
                }
                ?>
            </div>
        </div>
        <div class="page_m">
        <?php
        // PHẦN HIỂN THỊ PHÂN TRANG
        // nếu current_page > 1 và total_page > 1 mới hiển thị nút Trở về
        if ($current_page > 1 && $total_page > 1) {
            echo '<a href="index.php?pid=7&&page=' . ($current_page - 1) . '">Trở về</a> ';
        }

        // Lặp khoảng giữa
        for ($i = 1; $i <= $total_page; $i++) {
            // Nếu là trang hiện tại thì hiển thị thẻ span
            // ngược lại hiển thị thẻ a
            if ($i == $current_page) {
                echo '<button><span>' . $i . '</span></button> ';
            } else {
                echo '<button><a href="index.php?pid=7&&page=' . $i . '">' . $i . '</a></button> ';
            }
        }
        // nếu current_page < $total_page và total_page > 1 mới hiển thị nút trang tiếp theo
        if ($current_page < $total_page && $total_page > 1) {
            echo '<a href="index.php?pid=7&&page=' . ($current_page + 1) . '">Trang tiếp theo</a> ';
        }
        ?>
        </div>
    </section>
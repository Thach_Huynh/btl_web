-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 12, 2021 lúc 06:39 AM
-- Phiên bản máy phục vụ: 10.4.21-MariaDB
-- Phiên bản PHP: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `gundam`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `admin`
--

CREATE TABLE `admin` (
  `id` int(10) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(5, 'thach', '7b5dd5da9ee8f40b54853d0d8c901e34fad7056d');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill`
--

CREATE TABLE `bill` (
  `idBill` int(10) NOT NULL,
  `idCustomer` int(10) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` float NOT NULL,
  `status` int(10) NOT NULL,
  `payment` int(10) NOT NULL,
  `payment_status` int(10) NOT NULL,
  `dateBill` datetime NOT NULL,
  `Note` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bill`
--

INSERT INTO `bill` (`idBill`, `idCustomer`, `name`, `phone`, `email`, `address`, `total`, `status`, `payment`, `payment_status`, `dateBill`, `Note`) VALUES
(1, 3, 'Thach', '0984141206', 'thachhuynh@gmail.com', 'Khanh Hoa', 200000, 1, 1, 1, '2021-11-03 04:16:03', ''),
(2, 3, 't', '01212', '2424', '24242', 200000, 1, 0, 1, '2021-11-03 04:18:07', ''),
(3, 3, 't', '01212', '2424', '24242', 200000, 1, 0, 1, '2021-11-03 04:18:07', '');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `bill_detail`
--

CREATE TABLE `bill_detail` (
  `id` int(10) NOT NULL,
  `idBill` int(10) NOT NULL,
  `idMenu` int(10) NOT NULL,
  `quantity` int(10) NOT NULL,
  `price` float NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `bill_detail`
--

INSERT INTO `bill_detail` (`id`, `idBill`, `idMenu`, `quantity`, `price`) VALUES
(42, 14, 5, 10, 150000),
(47, 18, 2, 9, 17000);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customer`
--

CREATE TABLE `customer` (
  `idCustomer` int(10) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nameCustomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phoneCustomer` int(20) NOT NULL,
  `emailCustomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressCustomer` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `totalBill` int(10) NOT NULL,
  `typeCustomer` int(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customer`
--

INSERT INTO `customer` (`idCustomer`, `username`, `password`, `nameCustomer`, `phoneCustomer`, `emailCustomer`, `addressCustomer`, `totalBill`, `typeCustomer`) VALUES
(1, 'phuc', '9229ec79a248d8810d401bdf4febed623264819d', 'Trần Nguyễn Bảo Phúc', 986245128, 'baophuc@gmail.com', 'Tư Nghĩa, Quảng Ngãi', 0, 0),
(2, 'minh', 'b29025a795c4f5ea28fd2d7270a5600fe8099077', 'P', 987456129, 'minhpham@gmail.com', 'Gò Vấp, TP.HCM', 0, 0),
(3, 'thach', 'a20232a08cda98c9dfdcccf66b12d06e884a8a8c', 'Huỳnh Nguyên Thạch', 984141206, 'huynhthach@gmail.com', 'Cam Ranh, Khánh Hoà', 0, 0);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `employees`
--

CREATE TABLE `employees` (
  `idEmployees` int(11) NOT NULL,
  `nameEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imageEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `emailEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phoneEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `addressEmployees` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `employees`
--

INSERT INTO `employees` (`idEmployees`, `nameEmployees`, `imageEmployees`, `emailEmployees`, `phoneEmployees`, `addressEmployees`) VALUES
(2, 'Phạm Minh Triết ', 'NV001.png', 'trietpham2107@gmail.com', '0849277508', 'Thành Phố HCM');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu`
--

CREATE TABLE `menu` (
  `idMenu` int(10) NOT NULL,
  `nameMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `typeMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `priceMenu` float NOT NULL,
  `imageMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unitMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `noteMenu` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu`
--

INSERT INTO `menu` (`idMenu`, `nameMenu`, `typeMenu`, `priceMenu`, `imageMenu`, `unitMenu`, `noteMenu`) VALUES
(10, 'Gundam BARBATOS', 'HG', 1680000, 'Untitled.png', 'Bộ', 'Gundam siu xịn');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `bill`
--
ALTER TABLE `bill`
  ADD PRIMARY KEY (`idBill`);

--
-- Chỉ mục cho bảng `bill_detail`
--
ALTER TABLE `bill_detail`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bill_detailfbk1` (`idBill`),
  ADD KEY `menu_ibfk_1` (`idMenu`);

--
-- Chỉ mục cho bảng `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`idCustomer`);

--
-- Chỉ mục cho bảng `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`idEmployees`);

--
-- Chỉ mục cho bảng `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`idMenu`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `bill`
--
ALTER TABLE `bill`
  MODIFY `idBill` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT cho bảng `bill_detail`
--
ALTER TABLE `bill_detail`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT cho bảng `customer`
--
ALTER TABLE `customer`
  MODIFY `idCustomer` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT cho bảng `employees`
--
ALTER TABLE `employees`
  MODIFY `idEmployees` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `menu`
--
ALTER TABLE `menu`
  MODIFY `idMenu` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
